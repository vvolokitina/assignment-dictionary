%define list 0

%macro colon 2
    %%some_label:
    dq list
    db %1, 0

    %2:
    %define list %%some_label
%endmacro