section .text

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit:
    mov rax, 60
	xor rdi, rdi
	syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
.loop:
    cmp byte [rdi+rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
        push rdi
        call string_length
        pop rsi
        mov rdx, rax
        mov rax, 1
        mov rdi, 1
        syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    xor rax, rax
    push rdi
    mov rax, 1
    mov rdi, 1
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    call print_char
    ret

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
	push r9
	mov r9, rsp
	mov rax, rdi
	mov rdi, 10
	sub rsp, 128
	dec r9
	mov byte[r9], 0
.loop:
	dec r9
	xor rdx, rdx
	div rdi
	add rdx, 0x30
	mov byte[r9], dl
	test rax, rax
	jz .print
	jmp .loop
.print:
    mov rdi, r9
    call print_string
    add rsp, 128
    jmp .end
.end:
    pop r9
    ret

; Выводит знаковое 8-байтовое число в десятичном формате
print_int:
    xor rax,rax
    test rdi, rdi
    jge .print
    push rdi
    mov rdi, '-'
    call print_char
	pop rdi
	neg rdi
	jmp .print
.print:
    call print_uint
    ret

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    call string_length
	mov rcx, rax
	mov rdi, rsi
	push rdi
	call string_length
	pop rdi
	cmp rax, rcx
	jne .not_equals
.loop:
    cmp rcx, 0
    jbe .equals
    mov dl, byte[rdi + rcx - 1]
    cmp dl, byte[rsi + rcx - 1]
    jne .not_equals
    dec rcx
    jmp .loop
.not_equals:
    mov rax, 0
	ret
.equals:
    mov rax, 1
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rsi, rsp
    mov rdx, 1
    mov rax, 0
    mov rdi, 0
    syscall
    pop rax
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    mov r9, rdi
    mov r10, rsi
    xor rbx, rbx
.loop:
    call read_char
    cmp al, 0x20
    je .loop
    cmp al, 0x9
    je .loop
    cmp al, 0xA
    je .loop
    cmp al, 0
    je .end
    jmp .word_read
.word_read:
    mov byte[r9 + rbx], al
    inc rbx
    call read_char
    cmp al, 0x20
    je .end
    cmp al, 0x9
    je .end
    cmp al, 0xA
    je .end
    cmp al, 0
    je .end
    cmp r10, rbx
    ja .word_read
    jmp .overflow
.overflow:
    xor rax, rax
    pop rbx
    ret
.end:
    mov rax, r9
    mov rdx, rbx
    mov byte[r9 + rbx], 0
    pop rbx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
	xor rsi, rsi
	xor rcx, rcx
	mov rdx, 0
	mov r11, 10
.loop:
	mov sil, [rdi + rcx]
	cmp sil, '0'
	jl .end
	cmp sil, '9'
	jg .end
	inc rcx
	sub sil, '0'
	mul r11
	add rax, rsi
	jmp .loop
.end:
	mov rdx, rcx
    ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был)
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte[rdi], '-'
	jne parse_uint
	inc rdi
	call parse_uint
	neg rax
	inc rdx
	ret

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
	call string_length
	cmp rax, rdx
	jae .error
.loop:
    mov dl, byte[rdi]
    mov byte[rsi], dl
    inc rdi
    inc rsi
    cmp dl, 0
    jne .loop
    ret
.error:
    xor rax, rax
    ret
