%define buffer_length 256

global _start

section .text
%include "lib.inc"
%include "dict.inc"
%include "colon.inc"
%include "words.inc"

section .data
start_msg: db "Enter the key: ", 0
err_msg: db "Read error", 10, 0
not_found_msg: db "The key is not found", 10, 0
found_msg: db "Found value by the key: ", 0

_start:
    sub rsp, buffer_length
    mov rdi, start_msg
    call print_string
    mov rdi, rsp
    mov rsi, buffer_length
    push rdi
    call read_word
    pop rdi
    test rax, rax
    jz .error
    mov rsi, list
    call find_word
    add rsp, buffer_length
    test rax, rax
    jz .not_found
    push rax
    mov rdi, 1
    mov rdi, found_msg
    call print_string
    pop rax
    add rax, 8
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rsi
    mov rdi, 1
    call print_string
    call print_newline
    mov rdi, 0
    call exit

.error:
    add rsp, buffer_length
    mov rsi, err_msg
    call print_string
    mov rdi, 1
    call exit

.not_found:
    mov rdi, not_found_msg
    call print_string
    mov rdi, 1
    call exit