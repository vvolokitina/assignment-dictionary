ASM=nasm
ASMFLAGS=-felf64

main.o: main.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) $<
lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@
dict.o: dict.asm
	$(ASM) $(ASMFLAGS) -o $@
main: main.o dict.o lib.o
	ld -o $@ $^